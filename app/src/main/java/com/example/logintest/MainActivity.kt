package com.example.logintest

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.logintest.navigation.AppNavigation
import com.example.logintest.ui.theme.LoginTestTheme
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

/*
    PREGUNTAS
* Porque en color resource no se guarda el oapctiy
* Mira funciona con ambas, con la funcion denro de parametros y no (LoginScreen.kt)
    Como pasariamos el current user a la siguiente pantalla desde el navigation
*       Yo en LoginScreen me logeo y como le paso el objeto user a la siguiente pantalla
*
* Meter colores en archivos kotlin dentro de ui theme
* */

class MainActivity : ComponentActivity() {

    private lateinit var firebaseAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAuth = FirebaseAuth.getInstance()
        setContent {
            LoginTestTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background
                ) {
                    AppNavigation(firebaseAuth, this)
                }
            }
        }
    }

    fun comprobacion() {
        Log.d("MyApp", " ***  fun comprobacion()   iniciando...");

        firebaseAuth = FirebaseAuth.getInstance();
        //firebaseAuth.signInWithEmailAndPassword("prueba@correo.com", "prueba123")
        firebaseAuth.signInWithEmailAndPassword("zohotestzohotest323@gmail.com", "contraNueva123")
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    //texto = "BIEN"
                    Log.d("MyApp", "Usuario aceptado");
                } else {
                    //texto = "MAL"
                    Log.d("MyApp", "ERRROR");
                }
            }
        //registrar("zohotestzohotest323@gmail.com","contraNueva123")
    }

    fun verifyEmail(user: FirebaseUser) {
        user.sendEmailVerification().addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                Toast.makeText(this, "Email verificado", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Error al verificar email", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
