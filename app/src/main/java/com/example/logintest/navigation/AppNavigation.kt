package com.example.logintest.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.logintest.MainActivity
import com.example.logintest.screens.logeado.LogeadoScreen
import com.example.logintest.screens.login.LoginScreen
import com.example.logintest.screens.registro.RegistroScreen
import com.example.logintest.screens.login.LoginViewModel
import com.example.logintest.screens.registro.RegistroViewModel
import com.google.firebase.auth.FirebaseAuth

@Composable
fun AppNavigation(firebaseAuth: FirebaseAuth, activity: MainActivity) {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = AppScreens.LoginScreen.route) {
        composable(route = AppScreens.LoginScreen.route) {
            LoginScreen(LoginViewModel(), firebaseAuth, navController, activity)
        }

        composable(route = AppScreens.RegistroScreen.route) {
            RegistroScreen(RegistroViewModel(), firebaseAuth, navController, activity)
        }

        composable(route = AppScreens.LogeadoScreen.route) {
            LogeadoScreen(navController)
        }
    }
}