package com.example.logintest.navigation;

sealed class AppScreens (val route: String) {
    object LoginScreen : AppScreens("login_screen")
    object RegistroScreen : AppScreens("registro_screen")
    object LogeadoScreen : AppScreens("logeado_screen")
}