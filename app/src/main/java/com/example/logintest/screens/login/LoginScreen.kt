package com.example.logintest.screens.login

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.logintest.MainActivity
import com.example.logintest.R
import com.example.logintest.ui.theme.ColorApp1
import com.example.logintest.ui.theme.ColorApp2
import com.google.firebase.auth.FirebaseAuth


@Composable
fun LoginScreen(
    viewModel: LoginViewModel,
    firebaseAuth: FirebaseAuth,
    navController: NavController,
    activity: MainActivity
) {
    Box(
        Modifier
            .fillMaxSize()
            .padding(0.dp)
            // el color procedente del resource no coge los primeros FF que son la opacity
            .background(color = ColorApp2)
    ) {
        Login(
            Modifier.align(Alignment.Center), viewModel, firebaseAuth, navController, activity
        )
    }
}


@Composable
fun Login(
    modifier: Modifier,
    viewModel: LoginViewModel,
    firebaseAuth: FirebaseAuth,
    navController: NavController,
    activity: MainActivity
) {

    val email: String by viewModel.email.observeAsState("")
    val password: String by viewModel.password.observeAsState("")

    Column(modifier = modifier) {
        HeaderImage(Modifier.align(Alignment.CenterHorizontally))
        Spacer(modifier = Modifier.padding(5.dp))

        Text(text = "Login", color = ColorApp1, fontSize = 30.sp, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth())
        Spacer(modifier = Modifier.padding(14.dp))

        EmailField(email) { viewModel.onLoginChanged(it, password) }
        Spacer(modifier = Modifier.padding(16.dp))

        PasswordField(password) { viewModel.onLoginChanged(email, it) }
        Spacer(modifier = Modifier.padding(16.dp))

        RegistroYLoginBotones(viewModel, firebaseAuth, navController, activity)
    }
}

@Preview
@Composable
fun LoginButton() {
    Button(
        onClick = {

        }, modifier = Modifier
            .fillMaxWidth()
            .height(50.dp)
    ) {
        Text("Login")
    }
}

@Composable
fun RegistroYLoginBotones(
    viewModel: LoginViewModel,
    firebaseAuth: FirebaseAuth,
    navController: NavController,
    activity: MainActivity
) {
    Row(modifier = Modifier.fillMaxWidth()) {
        Button(
            onClick = {
                // CLICK REGISTRO
                navController.navigate("registro_screen")
            }, modifier = Modifier
                .height(60.dp)
                .padding(5.dp),
            colors = ButtonDefaults.buttonColors(containerColor = ColorApp1)
        ) {
            Text("Registro", color = ColorApp2, fontSize = 22.sp)
        }

        Button(
            onClick = {
                // CLICK LOGIN
                intentarLogear(viewModel, firebaseAuth, navController, activity)
            }, modifier = Modifier
                .fillMaxWidth()
                .height(60.dp)
                .padding(5.dp),
            colors = ButtonDefaults.buttonColors(containerColor = ColorApp1)
        ) {
            Text(text = "Login", color = ColorApp2, fontSize = 22.sp)
        }
    }
}

@Composable
fun EmailField(email: String, onTextFieldChange: (String) -> Unit) {
    // Como funciona esta abducion??
    TextField(
        value = email,
        onValueChange = { onTextFieldChange(it) },
        modifier = Modifier
            .fillMaxWidth()
            .height(60.dp)
            .padding(horizontal = 15.dp),
        placeholder = { Text("Email") },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email),
        singleLine = true,
        maxLines = 1
    )
}

@Composable
fun PasswordField(password: String, onTextFieldChange: (String) -> Unit) {
    TextField(
        value = password,
        onValueChange = { onTextFieldChange(it) },
        modifier = Modifier
            .fillMaxWidth()
            .height(60.dp)
            .padding(horizontal = 15.dp),
        placeholder = { Text("Password") },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
        singleLine = true,
        maxLines = 1
    )
}

@Composable
fun HeaderImage(modifier: Modifier) {
    Image(
        painter = painterResource(id = R.drawable.logo),
        contentDescription = "Header Image",
        modifier = modifier
    )
}

fun intentarLogear(
    viewModel: LoginViewModel,
    firebaseAuth: FirebaseAuth,
    navController: NavController,
    activity: MainActivity
) {
    // em@ail.com - email12
    firebaseAuth.signInWithEmailAndPassword(
        viewModel.email.value.toString(),
        viewModel.password.value.toString()
    )
        .addOnCompleteListener(activity) { task ->
            if (task.isSuccessful) {
                navController.navigate("logeado_screen")
            } else {
                //texto = "MAL"
                Log.d("MyApp", "ERRROR");
            }
        }
}