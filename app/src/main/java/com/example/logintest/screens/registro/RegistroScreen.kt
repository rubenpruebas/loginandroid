package com.example.logintest.screens.registro

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.logintest.MainActivity
import com.example.logintest.R
import com.example.logintest.ui.theme.ColorApp1
import com.example.logintest.ui.theme.ColorApp2
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

@Composable
fun RegistroScreen(
    viewModel: RegistroViewModel,
    firebaseAuth: FirebaseAuth,
    navController: NavController,
    activity: MainActivity
) {
    Box(
        Modifier
            .fillMaxSize()
            .padding(0.dp)
            // el color procedente del resource no coge los primeros FF que son la opacity
            .background(color = Color(0xFF333333))
    ) {
        Registro(
            Modifier.align(Alignment.Center), viewModel, firebaseAuth, navController, activity
        )
    }
}

@Composable
fun Registro(
    modifier: Modifier,
    viewModel: RegistroViewModel,
    firebaseAuth: FirebaseAuth,
    navController: NavController,
    activity: MainActivity
) {
    val email: String by viewModel.email.observeAsState("")
    val password: String by viewModel.password.observeAsState("")
    val nombre: String by viewModel.nombre.observeAsState("")
    val apellido: String by viewModel.apellido.observeAsState("")

    Column(modifier = modifier) {
        HeaderImage(Modifier.align(Alignment.CenterHorizontally))
        Spacer(modifier = Modifier.padding(5.dp))

        Text(text = "Registro", color = ColorApp1, fontSize = 30.sp, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth())
        Spacer(modifier = Modifier.padding(14.dp))

        EmailField(email) {
            viewModel.onRegistroChanged(it, password, nombre, apellido)
        }
        Spacer(modifier = Modifier.padding(16.dp))

        PasswordField(password) {
            viewModel.onRegistroChanged(email, it, nombre, apellido)
        }
        Spacer(modifier = Modifier.padding(16.dp))

        NombreField(nombre) {
            viewModel.onRegistroChanged(email, password, it, apellido)
        }
        Spacer(modifier = Modifier.padding(16.dp))

        ApellidoField(apellido) {
            viewModel.onRegistroChanged(email, password, nombre, it)
        }
        Spacer(modifier = Modifier.padding(16.dp))

        LoginYRegistroBotones(viewModel, firebaseAuth, navController, activity)
    }
}

@Composable
fun EmailField(email: String, onTextFieldChange: (String) -> Unit) {
    // Como funciona esta abducion??
    TextField(
        value = email,
        onValueChange = { onTextFieldChange(it) },
        modifier = Modifier
            .fillMaxWidth()
            .height(60.dp)
            .padding(horizontal = 15.dp),
        placeholder = { Text("Email") },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email),
        singleLine = true,
        maxLines = 1
    )
}

@Composable
fun NombreField(nombre: String, onTextFieldChange: (String) -> Unit) {
    // Como funciona esta abducion??
    TextField(
        value = nombre,
        onValueChange = { onTextFieldChange(it) },
        modifier = Modifier
            .fillMaxWidth()
            .height(60.dp)
            .padding(horizontal = 15.dp),
        placeholder = { Text("Nombre") },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
        singleLine = true,
        maxLines = 1
    )
}

@Composable
fun ApellidoField(apellido: String, onTextFieldChange: (String) -> Unit) {
    // Como funciona esta abducion??
    TextField(
        value = apellido,
        onValueChange = { onTextFieldChange(it) },
        modifier = Modifier
            .fillMaxWidth()
            .height(60.dp)
            .padding(horizontal = 15.dp),
        placeholder = { Text("Apellido") },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
        singleLine = true,
        maxLines = 1
    )
}

@Composable
fun PasswordField(password: String, onTextFieldChange: (String) -> Unit) {
    TextField(
        value = password,
        onValueChange = { onTextFieldChange(it) },
        modifier = Modifier
            .fillMaxWidth()
            .height(60.dp)
            .padding(horizontal = 15.dp),
        placeholder = { Text("Password") },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
        singleLine = true,
        maxLines = 1
    )
}

@Composable
fun HeaderImage(modifier: Modifier) {
    Image(
        painter = painterResource(id = R.drawable.logo),
        contentDescription = "Header Image",
        modifier = modifier
    )
}

@Composable
fun LoginYRegistroBotones(
    viewModel: RegistroViewModel,
    firebaseAuth: FirebaseAuth,
    navController: NavController,
    activity: MainActivity
) {
    Row(modifier = Modifier.fillMaxWidth()) {
        Button(
            onClick = {
                // Click en login
                navController.navigate("login_screen")
            }, modifier = Modifier
                .height(60.dp)
                .padding(5.dp),
            colors = ButtonDefaults.buttonColors(containerColor = ColorApp1)
        ) {
            Text("Login", color = ColorApp2, fontSize = 22.sp)
        }

        Button(
            onClick = {
                // Click en registrar
                intentarRegistrar(viewModel, firebaseAuth, navController, activity)
            }, modifier = Modifier
                .fillMaxWidth()
                .height(60.dp)
                .padding(5.dp),
            colors = ButtonDefaults.buttonColors(containerColor = ColorApp1)
        ) {
            Text(text = "Registrar", color = ColorApp2, fontSize = 22.sp)
        }
    }
}

fun intentarRegistrar(
    viewModel: RegistroViewModel,
    firebaseAuth: FirebaseAuth,
    navController: NavController,
    activity: MainActivity
) {

    // Eliminada comprobacion de campos vacios al registrar
    // Damos de alta email y pass
    firebaseAuth.createUserWithEmailAndPassword(
        viewModel.email.value.toString(),
        viewModel.password.value.toString()
    ).addOnCompleteListener(activity) {
        // Obtener el id de usuario, sabemos que no es nulo ya que lo acabamos de crear
        val user: FirebaseUser = firebaseAuth.currentUser!!
        // VerifyEmail(user)
        navController.navigate("logeado_screen")
    }.addOnFailureListener {
        Log.d("MyApp", "Error al crear usuario")
    }
}

/*
fun  verifyEmail(user: FirebaseUser):Boolean?{
    user.sendEmailVerification().addOnCompleteListener(this){
            task->
        if(task.isSuccessful){
            return true // aqui no se puede poner un return, seria con una callback segun stackoverflow
            Toast.makeText(this, "Email verificado", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(this, "Error al verificar email", Toast.LENGTH_SHORT).show()
        }
        return true
    }
}
*/

