package com.example.logintest.screens.logeado

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.logintest.R
import com.example.logintest.ui.theme.ColorApp1
import com.example.logintest.ui.theme.ColorApp2

@Composable
fun LogeadoScreen(navController: NavController) {
    Box(
        Modifier
            .fillMaxSize()
            .padding(0.dp)
            .background(color = ColorApp2)
    )
    {
        Logeado(Modifier.align(Alignment.Center), navController = navController)
    }
}

@Composable
fun Logeado(modifier: Modifier, navController: NavController) {
    Column(modifier = modifier) {
        HeaderImage(modifier = Modifier.align(Alignment.CenterHorizontally))
        Spacer(modifier = Modifier.padding(7.dp))
        Text(
            text = "Logeado!!!",
            color = ColorApp1,
            fontSize = 40.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
fun HeaderImage(modifier: Modifier) {
    Image(
        painter = painterResource(id = R.drawable.logo),
        contentDescription = "Header Image",
        modifier = modifier
    )
}